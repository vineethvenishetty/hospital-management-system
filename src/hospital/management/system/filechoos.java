/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hospital.management.system;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.*;    
import java.awt.event.*;    
import java.io.*;    
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import javax.swing.JLabel;

import java.awt.image.*;
import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import static javax.swing.JFrame.EXIT_ON_CLOSE;


public class filechoos extends JFrame implements ActionListener {
JMenuBar mb;    
JMenu file;    
JMenuItem open;    
JTextArea ta; 
JLabel jl;
JLabel jp;
BufferedImage img1=null;
BufferedImage img2=null;
BufferedImage img3=null; 
File f= null;
File f2= null;
File f3 = null; 
filechoos()
{    
open=new JMenuItem("Open File");    
open.addActionListener(this);            
file=new JMenu("File");    
file.add(open);
jl =new JLabel();
jl.setBounds(0,0,500,100);
jp = new JLabel();
jp.setBounds(200,200,600,600);
mb=new JMenuBar();    
mb.setBounds(0,0,800,20);    
mb.add(file);              
ta=new JTextArea(0,0);
ta.setEditable(false); 
ta.setBounds(50,20,8,8);   
add(jl);
add(mb);    
add(ta);    
add(jp);
}
 public static BufferedImage getDifferenceImage(BufferedImage img1, BufferedImage img2) {
    int width1 = img1.getWidth(); // Change - getWidth() and getHeight() for BufferedImage
    int width2 = img2.getWidth(); // take no arguments
    int height1 = img1.getHeight();
    int height2 = img2.getHeight();
    if ((width1 !=width2) || (height1 != height2)) {
        System.out.print("image 1 dimensions:"+width1+height1);
        System.out.print("image 1 dimensions:"+width2+height2);
        JOptionPane.showMessageDialog(null,"Error: Images dimensions mismatch");
        System.err.println("Error: Images dimensions mismatch");
        System.exit(1);
    }

    // NEW - Create output Buffered image of type RGB
    BufferedImage outImg = new BufferedImage(width1, height1, BufferedImage.TYPE_INT_RGB);

    // Modified - Changed to int as pixels are ints
    int diff;
    int result; // Stores output pixel
    for (int i = 0; i < height1; i++) {
        for (int j = 0; j < width1; j++) {
            int rgb1 = img1.getRGB(j, i);
            int rgb2 = img2.getRGB(j, i);
            int r1 = (rgb1 >> 16) & 0xff;
            int g1 = (rgb1 >> 8) & 0xff;
            int b1 = (rgb1) & 0xff;
            int r2 = (rgb2 >> 16) & 0xff;
            int g2 = (rgb2 >> 8) & 0xff;
            int b2 = (rgb2) & 0xff;
            diff = Math.abs(r1 - r2); // Change
            diff += Math.abs(g1 - g2);
            diff += Math.abs(b1 - b2);
            diff /= 3; // Change - Ensure result is between 0 - 255
             
            // Make the difference image gray scale
            // The RGB components are all the same
            result = (diff << 16) | (diff << 8) | diff;
            outImg.setRGB(j, i, result); // Set result
        }
    }
   

    // Now return
    return outImg;

    /**
     * @param args the command line arguments
     */

}
  public void actionPerformed(ActionEvent e) {
        
      
            
       JFileChooser jf = new JFileChooser();
       int i=jf.showOpenDialog(null);
       if(i==JFileChooser.APPROVE_OPTION){    
        try{    
             Scanner scan = new Scanner(new FileReader(jf.getSelectedFile().getPath()));
             f=jf.getSelectedFile();
             f2=new File("C:\\Users\\Vineeth Venishetty\\Desktop\\pneumonia.jpg");
             f3= new File("C:\\Users\\Vineeth Venishetty\\Desktop\\out.png");
             img1=ImageIO.read(f);
             img2=ImageIO.read(f2);
        
             img3=getDifferenceImage(img1,img2);
             ImageIO.write(img3, "png", f3);
             System.out.println("You chose to open this file: " + jf.getSelectedFile().getName());
             System.out.print("directorio:"+jf.getCurrentDirectory());
             String cad;
             cad=jf.getCurrentDirectory()+"/"+jf.getSelectedFile().getName();
             jl.setText(cad);
             jp.setIcon(new ImageIcon(cad));
             
        }
        catch(FileNotFoundException s)
        {
            
        }  catch (IOException ex) {
               Logger.getLogger(filechoos.class.getName()).log(Level.SEVERE, null, ex);
           }}
 
       
    }
  
   

   
  
    
}
